package simplycodinghub.learnandroidapp.CustomBottomNavigation.WithoutLibrary;

import android.app.Fragment;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import simplycodinghub.learnandroidapp.R;

public class Main52Activity extends AppCompatActivity {
    BottomNavigationView navigation;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main52);

        navigation = (BottomNavigationView) findViewById(R.id.bottom_nav);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.home:
                    Toast.makeText(Main52Activity.this, "home", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.favourite:
                    Toast.makeText(Main52Activity.this, "favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.settings:
                    Toast.makeText(Main52Activity.this, "settings", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.search:
                    Toast.makeText(Main52Activity.this, "search", Toast.LENGTH_SHORT).show();
                    return true;
            }
            return false;
        }
    };
}
