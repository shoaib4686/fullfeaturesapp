package simplycodinghub.learnandroidapp.RoomDatabasewithRetrofit.ViewModal;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import simplycodinghub.learnandroidapp.RoomDatabasewithRetrofit.Modal.Posts;
import simplycodinghub.learnandroidapp.RoomDatabasewithRetrofit.Repository.Repository;

public class PostViewModal extends AndroidViewModel {
    private Repository repository;
    public LiveData<List<Posts>> getAllPosts;

    public PostViewModal(@NonNull Application application) {
        super(application);
        repository=new Repository(application);
        getAllPosts=repository.getAllPosts();
    }

    public void insert(List<Posts> posts){
        repository.insert(posts);
    }

    public LiveData<List<Posts>> getAllPosts()
    {
        return getAllPosts;
    }
}
