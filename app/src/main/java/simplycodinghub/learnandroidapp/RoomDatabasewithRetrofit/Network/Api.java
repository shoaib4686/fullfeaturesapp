package simplycodinghub.learnandroidapp.RoomDatabasewithRetrofit.Network;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import simplycodinghub.learnandroidapp.RoomDatabasewithRetrofit.Modal.Posts;

public interface Api {
    @GET("/data.php")
    Call<List<Posts>>  getPosts();
}
