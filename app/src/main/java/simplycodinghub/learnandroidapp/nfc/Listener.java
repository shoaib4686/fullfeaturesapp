package simplycodinghub.learnandroidapp.nfc;

public interface Listener {

    void onDialogDisplayed();

    void onDialogDismissed();
}
