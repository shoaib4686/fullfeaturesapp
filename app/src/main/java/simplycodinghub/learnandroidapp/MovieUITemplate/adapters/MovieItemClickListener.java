package simplycodinghub.learnandroidapp.MovieUITemplate.adapters;

import android.widget.ImageView;

import simplycodinghub.learnandroidapp.MovieUITemplate.models.Movie;


public interface MovieItemClickListener {

    void onMovieClick(Movie movie, ImageView movieImageView); // we will need the imageview to make the shared animation between the two activity

}
