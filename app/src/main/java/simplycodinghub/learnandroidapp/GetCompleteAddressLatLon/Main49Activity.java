package simplycodinghub.learnandroidapp.GetCompleteAddressLatLon;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import simplycodinghub.learnandroidapp.R;


public class Main49Activity extends AppCompatActivity implements LocationListener {
    String strAdd = "",GPS_starting_point,provider;
    TextView tv_getAddress;
    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    Location location;
    String latitude, longitude;
    private double currentLatitude;
    private double currentLongitude;
    String mPermission = android.Manifest.permission.ACCESS_FINE_LOCATION;
    GpsTrackers gps;
    final Context context = this;
    String mprovider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main49);
       tv_getAddress = findViewById(R.id.tv_getAddress);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        mprovider = locationManager.getBestProvider(criteria, false);

        if (mprovider != null && !mprovider.equals("")) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = locationManager.getLastKnownLocation(mprovider);
            locationManager.requestLocationUpdates(mprovider, 15000, 1, this);

            if (location != null)
                onLocationChanged(location);
            else
                Toast.makeText(getBaseContext(), "No Location Provider Found Check Your Code", Toast.LENGTH_SHORT).show();
        }

    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address rAddress = addresses.get(0);
                StringBuilder sAddress = new StringBuilder("");

                for (int i = 0; i <= rAddress.getMaxAddressLineIndex(); i++) {
                    sAddress.append(rAddress.getAddressLine(i)).append("\n");
                    sAddress.append(rAddress.getCountryCode()).append("\n");
                    sAddress.append(rAddress.getPostalCode()).append("\n");
                    sAddress.append(rAddress.getAdminArea()).append("\n");
                    sAddress.append(rAddress.getLatitude());
                    sAddress.append(rAddress.getLongitude());
                    //Address_starting_point = rAddress.getAddressLine(i);
                    double startlat1 = rAddress.getLatitude();
                    double startlon1 = rAddress.getLongitude();
                    GPS_starting_point = startlat1 + "," + startlon1;
                    // Toast.makeText(getActivity(), "aaaaaa"+GPS_starting_point+ address_starting_point+startlat1+startlon1, Toast.LENGTH_SHORT).show();
                }
                strAdd = sAddress.toString();
                 Toast.makeText(this, "" + strAdd, Toast.LENGTH_SHORT).show();
                tv_getAddress.setText(strAdd);
                // Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
                //Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;

    }


    @Override
    public void onLocationChanged(Location location) {
        tv_getAddress = findViewById(R.id.tv_getAddress);
        tv_getAddress.setText("Current Latitude:" + location.getLatitude());
        getCompleteAddressString(location.getLatitude(),location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
