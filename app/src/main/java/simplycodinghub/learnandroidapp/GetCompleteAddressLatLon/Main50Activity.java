package simplycodinghub.learnandroidapp.GetCompleteAddressLatLon;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import simplycodinghub.learnandroidapp.R;

public class Main50Activity extends AppCompatActivity {
    GpsTrackers gpsTrackers;
    String strAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main50);
        gpsTrackers = new GpsTrackers(this);

        gpsTrackers.getLocation();
        gpsTrackers.getLatitude();

        Log.d("GpsTrackers", "" + gpsTrackers.getLatitude());

        Log.d("GpsTrackers", "" + gpsTrackers.getLongitude());

        getCompleteAddressString(gpsTrackers.getLatitude(),gpsTrackers.getLongitude());
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address rAddress = addresses.get(0);
                StringBuilder sAddress = new StringBuilder("");

                for (int i = 0; i <= rAddress.getMaxAddressLineIndex(); i++) {
                    sAddress.append(rAddress.getAddressLine(i)).append("\n");
                    sAddress.append(rAddress.getCountryCode()).append("\n");
                    sAddress.append(rAddress.getPostalCode()).append("\n");
                    sAddress.append(rAddress.getAdminArea()).append("\n");
                    sAddress.append(rAddress.getLatitude());
                    sAddress.append(rAddress.getLongitude());
                    //Address_starting_point = rAddress.getAddressLine(i);
                    double startlat1 = rAddress.getLatitude();
                    double startlon1 = rAddress.getLongitude();
                }
                strAdd = sAddress.toString();
                Toast.makeText(this, "" + strAdd, Toast.LENGTH_SHORT).show();
                //tv_getAddress.setText(strAdd);
                 Log.d("GpsTrackers",strAdd);
            } else {
                //Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }
}
