package simplycodinghub.learnandroidapp.DataBindingRecyclerView;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simplycodinghub.learnandroidapp.DataBindingRecyclerView.retrofit.RetrofitClient;
import simplycodinghub.learnandroidapp.DataBindingRecyclerView.EmployeeDataService;
import simplycodinghub.learnandroidapp.Employee;
import simplycodinghub.learnandroidapp.EmployeeDBResponse;

public class EmployeeRepository {

    private static final String TAG = "EmployeeRepository";
    private ArrayList<Employee> employees = new ArrayList<>();
    private MutableLiveData<List<Employee>> mutableLiveData = new MutableLiveData<>();

    /*
    8 - STEPS OF UNDERSTANDING REPOSITORY
    * Repository used for Network call and provide instance of MutableLive Data
    * Create Mutable method
    * Combine BASE AND END URL
    * Set response to model and call getEmployees using Call<T>
    * call.enqueue
    * onResponse assign response.body to EmployeeDBResponse (Model)
    * Check null and Cast response to ArrayList
    * Set ArrayList object to MutableLiveData object
    * */

    public MutableLiveData<List<Employee>> getMutableLiveData() {
        final EmployeeDataService userDataService = RetrofitClient.getService();
        Call<EmployeeDBResponse> call = userDataService.getEmployees();
        call.enqueue(new Callback<EmployeeDBResponse>() {
            @Override
            public void onResponse(Call<EmployeeDBResponse> call, Response<EmployeeDBResponse> response) {
                EmployeeDBResponse employeeDBResponse = response.body();
                if (employeeDBResponse != null && employeeDBResponse.getEmployee() != null) {
                    employees = (ArrayList<Employee>) employeeDBResponse.getEmployee();
                    mutableLiveData.setValue(employees);
                }
            }
            @Override
            public void onFailure(Call<EmployeeDBResponse> call, Throwable t) {
            }
        });
        return mutableLiveData;
    }

}
