package simplycodinghub.learnandroidapp.DataBindingRecyclerView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import simplycodinghub.learnandroidapp.Employee;
import simplycodinghub.learnandroidapp.R;
import simplycodinghub.learnandroidapp.databinding.ActivityMain40Binding;

public class Main40Activity extends AppCompatActivity {

    /*
    4 STEPS of MAIN ACTIVITY
    * Create Instance of ViewModel and Adapter
    * Bind Recyclerview
    * set ViewModel
    * Fetch List and update using observer
    * */


    private MainViewModel mainViewModel;
    private EmployeeDataAdapter employeeDataAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMain40Binding activityMainBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_main40);

        // bind RecyclerView
        RecyclerView recyclerView = activityMainBinding.viewEmployees;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        employeeDataAdapter = new EmployeeDataAdapter();
        recyclerView.setAdapter(employeeDataAdapter);
        getAllEmployee();
    }


    private void getAllEmployee() {
        mainViewModel.getAllEmployee().observe(this, new Observer<List<Employee>>() {
            @Override
            public void onChanged(@Nullable List<Employee> employees) {
                employeeDataAdapter.setEmployeeList((ArrayList<Employee>) employees);
            }
        });
    }
}
