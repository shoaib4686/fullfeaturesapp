package simplycodinghub.learnandroidapp.DataBindingRecyclerView;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import simplycodinghub.learnandroidapp.DataBindingRecyclerView.EmployeeRepository;
import simplycodinghub.learnandroidapp.Employee;

public class MainViewModel extends AndroidViewModel {
    /*
    2 STEPS OF VIEW MODEL
    * Create global object of Repository inside Constructor
    * Create LiveData method and call method of Repository;
    * */

    private EmployeeRepository employeeRepository;

    public MainViewModel(@NonNull Application application) {
        super(application);

        employeeRepository = new EmployeeRepository();
    }

    public LiveData<List<Employee>> getAllEmployee() {

        return employeeRepository.getMutableLiveData();
    }

}
