package simplycodinghub.learnandroidapp.AProjects.Home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;

import simplycodinghub.learnandroidapp.AProjects.Home.Adapters.FoodAdapter;
import simplycodinghub.learnandroidapp.AProjects.Home.Model.FoodModel;
import simplycodinghub.learnandroidapp.R;

public class HomeActivity extends AppCompatActivity {

    CarouselView carouselView;
    int[] sampleImages = {R.drawable.app_images_diwali1, R.drawable.app_images_diwali4, R.drawable.app_images_diwali5, R.drawable.app_images_diwali2};

    private ArrayList<FoodModel> mHomeListModelClassArrayList;
    private RecyclerView mRecyclerView, mRecyclerView2, mRecyclerView3;
    private FoodAdapter mAdapter;
    Integer image[] = {R.drawable.cat_46, R.drawable.cat_46_blue, R.drawable.cat_46, R.drawable.cat_46_blue, R.drawable.cat_46, R.drawable.cat_46_blue};
    String foodName[] = {"Fast Food", "Chineese", "Punjabi", "Fast Food", "Chineese", "Punjabi"};
    String totalRest[] = {"74 Restaurant", "34 Restaurant", "65 Restaurant", "74 Restaurant", "34 Restaurant", "65 Restaurant"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        /*CAROSUEL*/
        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);
        carouselView.setImageListener(imageListener);


        /*RecyclerView 1 */
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mHomeListModelClassArrayList = new ArrayList<>();
        for (int i = 0; i < image.length; i++) {
            FoodModel beanClassForRecyclerView_contacts = new FoodModel(image[i], foodName[i], totalRest[i]);
            mHomeListModelClassArrayList.add(beanClassForRecyclerView_contacts);
        }
        mAdapter = new FoodAdapter(HomeActivity.this, mHomeListModelClassArrayList);
        GridLayoutManager mLayoutManager = new GridLayoutManager(HomeActivity.this, 3, GridLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);


        /*RecyclerView 2 */
        mRecyclerView2 = (RecyclerView) findViewById(R.id.recyclerView2);
        mHomeListModelClassArrayList = new ArrayList<>();
        for (int i = 0; i < image.length; i++) {
            FoodModel beanClassForRecyclerView_contacts = new FoodModel(image[i], foodName[i], totalRest[i]);
            mHomeListModelClassArrayList.add(beanClassForRecyclerView_contacts);
        }
        mAdapter = new FoodAdapter(HomeActivity.this, mHomeListModelClassArrayList);
        GridLayoutManager mLayoutManager2 = new GridLayoutManager(HomeActivity.this, 3, GridLayoutManager.VERTICAL, false);
        mRecyclerView2.setLayoutManager(mLayoutManager2);
        mRecyclerView2.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView2.setAdapter(mAdapter);


        /*RecyclerView 3 */
        mRecyclerView3 = (RecyclerView) findViewById(R.id.recyclerView3);
        mHomeListModelClassArrayList = new ArrayList<>();
        for (int i = 0; i < image.length; i++) {
            FoodModel beanClassForRecyclerView_contacts = new FoodModel(image[i], foodName[i], totalRest[i]);
            mHomeListModelClassArrayList.add(beanClassForRecyclerView_contacts);
        }
        mAdapter = new FoodAdapter(HomeActivity.this, mHomeListModelClassArrayList);
        GridLayoutManager mLayoutManager3 = new GridLayoutManager(HomeActivity.this, 3, GridLayoutManager.VERTICAL, false);
        mRecyclerView3.setLayoutManager(mLayoutManager3);
        mRecyclerView3.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView3.setAdapter(mAdapter);

        //Layout onClickListener
        LinearLayout ll_outdoor = findViewById(R.id.ll_outdoor);
        ll_outdoor.setOnClickListener((View v) -> {
            // do something here
            Intent i = new Intent(HomeActivity.this, CategoryListActivity.class);
            HomeActivity.this.startActivity(i);
        });

    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };
}
