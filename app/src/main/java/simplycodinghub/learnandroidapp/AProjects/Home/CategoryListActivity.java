package simplycodinghub.learnandroidapp.AProjects.Home;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import simplycodinghub.learnandroidapp.AProjects.Home.Adapters.CategoryAdapter2;
import simplycodinghub.learnandroidapp.R;

public class CategoryListActivity extends AppCompatActivity {
    String[] category2222 = {"t shirt", "video", "print", "card print", "events", "banner", "poster", "standy", "flex"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        RecyclerView categoryList = (RecyclerView) findViewById(R.id.RecyclerViewCategory);
        categoryList.setLayoutManager(new LinearLayoutManager(this));
        categoryList.setAdapter(new CategoryAdapter2(category2222));
    }
}
