package simplycodinghub.learnandroidapp.AProjects.Home.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import simplycodinghub.learnandroidapp.R;

public class CategoryAdapter2 extends RecyclerView.Adapter<CategoryAdapter2.CategoryViewHolder> {

    private String[] data;

    public CategoryAdapter2(String[] data) {
        this.data = data;
    }


    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflater = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_layout, parent, false);
        return new CategoryViewHolder(inflater);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        String title = data[position];
        holder.tv_text.setText(title);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tv_text;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img);
            tv_text = (TextView) itemView.findViewById(R.id.tv_text);
        }
    }


}