package simplycodinghub.learnandroidapp.AProjects.Home.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import simplycodinghub.learnandroidapp.MusicApp.AlbumFragment;
import simplycodinghub.learnandroidapp.MusicApp.ArtistFragment;
import simplycodinghub.learnandroidapp.MusicApp.SongsFragment;


/**
 * Created by praja on 06-06-17.
 */

public class CategoryAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 4;
    private String tabTitles[] = new String[] { "ALBUMS", "SONGS", "ARTISTS", "PLAYLISTS"};



    public CategoryAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){

            case 0:

                AlbumFragment albumFragment = new AlbumFragment();
                return albumFragment;
            case 1:

                SongsFragment songsFragment = new SongsFragment();
                return songsFragment;
            case 2:
                ArtistFragment artistFragment = new ArtistFragment();
                return artistFragment;
            case 3:

                AlbumFragment albumFragment4 = new AlbumFragment();
                return albumFragment4;

        }

        return null;


    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}