package simplycodinghub.learnandroidapp.MusicApp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import simplycodinghub.learnandroidapp.AProjects.Home.Adapters.CategoryAdapter;
import simplycodinghub.learnandroidapp.R;
import simplycodinghub.learnandroidapp.SlidingActivity;

public class Main39Activity extends AppCompatActivity implements View.OnClickListener {

    TabLayout tabs;
    ViewPager viewPager;
    CategoryAdapter categoryAdapter;
    LinearLayout linear;
    ImageView play, pause;
    private Typeface mTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main39);

        /* ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(false);
       */

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        linear = (LinearLayout) findViewById(R.id.linear);

        categoryAdapter = new CategoryAdapter(getSupportFragmentManager());
        viewPager.setAdapter(categoryAdapter);
        tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(3);

        play = (ImageView) findViewById(R.id.play);
        pause = (ImageView) findViewById(R.id.pause);

        play.setOnClickListener(this);
        pause.setOnClickListener(this);

        linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), SlidingActivity.class);
                startActivity(i);
            }
        });


        mTypeface = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue Medium.ttf");
        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(mTypeface, Typeface.NORMAL);
                }
            }
        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.play:

                pause.setVisibility(View.VISIBLE);
                play.setVisibility(View.GONE);

                break;

            case R.id.pause:

                play.setVisibility(View.VISIBLE);
                pause.setVisibility(View.GONE);
                break;

        }


    }
}
