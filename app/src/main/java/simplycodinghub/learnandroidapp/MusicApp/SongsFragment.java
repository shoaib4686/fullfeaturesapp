package simplycodinghub.learnandroidapp.MusicApp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import simplycodinghub.learnandroidapp.MusicApp.Adapter.SongAdapter;
import simplycodinghub.learnandroidapp.MusicApp.ModalClass.SongModalClass;
import simplycodinghub.learnandroidapp.R;

/**
 * Created by Remmss on 28-08-2017.
 */

public class SongsFragment extends Fragment {

    RecyclerView recycle;
    private List<SongModalClass> songModalClasses = new ArrayList<>();
    private SongAdapter songAdapter;
    ImageView play,pause;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_song, container, false);


        recycle = (RecyclerView) view.findViewById(R.id.recycle);
       /* play= (ImageView) view.findViewById(R.id.play);
        pause= (ImageView) view.findViewById(R.id.pause);

        play.setOnClickListener(this);
        pause.setOnClickListener(this);*/



        songAdapter = new SongAdapter(songModalClasses);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycle.setLayoutManager(mLayoutManager);
        recycle.setItemAnimator(new DefaultItemAnimator());
        recycle.setAdapter(songAdapter);

        prepareMovieData();

        return view;
    }

    private void prepareMovieData() {

        SongModalClass modalClass = new SongModalClass(R.drawable.ic_gray,"Baby Ko Bass Pasand Hai","Vishal Dadlan","Sultan","4:50");
        songModalClasses.add(modalClass);

        SongModalClass modalClass1 = new SongModalClass(R.drawable.ic_gray,"Because I Love You","Unknown Artist","Album","4:32");
        songModalClasses.add(modalClass1);

        SongModalClass modalClass2 = new SongModalClass(R.drawable.ic_gray,"Bezubaan","Anypan Roy","Piku","5:07");
        songModalClasses.add(modalClass2);

        SongModalClass modalClass3 = new SongModalClass(R.drawable.ic_gray,"Chaar Kadam","Shaan & Shreya Goshal","PK","4:50");
        songModalClasses.add(modalClass3);

        SongModalClass modalClass4 = new SongModalClass(R.drawable.ic_gray,"Daaru Peeke Dance","Neha Kakkar","Kuch Kuch...","4:32");
        songModalClasses.add(modalClass4);

        SongModalClass modalClass5 = new SongModalClass(R.drawable.ic_gray,"Der Lagi Lekin","Shankar Mahadeven","Zindagi Na..","5:07");
        songModalClasses.add(modalClass5);

        SongModalClass modalClass6 = new SongModalClass(R.drawable.ic_gray,"Baby Ko Bass Pasand Hai","Vishal Dadlan","Sultan","4:50");
        songModalClasses.add(modalClass6);

        SongModalClass modalClass7 = new SongModalClass(R.drawable.ic_gray,"Because I Love You","Unknown Artist","Album","4:32");
        songModalClasses.add(modalClass7);

        SongModalClass modalClass8 = new SongModalClass(R.drawable.ic_gray,"Bezubaan","Anypan Roy","Piku","5:07");
        songModalClasses.add(modalClass8);

        SongModalClass modalClass10 = new SongModalClass(R.drawable.ic_gray,"Chaar Kadam","Shaan & Shreya Goshal","PK","4:50");
        songModalClasses.add(modalClass10);

        SongModalClass modalClass11 = new SongModalClass(R.drawable.ic_gray,"Daaru Peeke Dance","Neha Kakkar","Kuch Kuch...","4:32");
        songModalClasses.add(modalClass11);

        SongModalClass modalClass12 = new SongModalClass(R.drawable.ic_gray,"Der Lagi Lekin","Shankar Mahadeven","Zindagi Na..","5:07");
        songModalClasses.add(modalClass12);





        songAdapter.notifyDataSetChanged();
    }

   /* @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.play:

                pause.setVisibility(View.VISIBLE);
                play.setVisibility(View.GONE);

                break;

            case R.id.pause:

                play.setVisibility(View.VISIBLE);
                pause.setVisibility(View.GONE);
                break;

        }
    }*/
}