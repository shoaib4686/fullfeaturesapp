package simplycodinghub.learnandroidapp.MusicApp.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import simplycodinghub.learnandroidapp.MusicApp.ModalClass.SongModalClass;
import simplycodinghub.learnandroidapp.R;

/**
 * Created by Remmss on 29-08-2017.
 */

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.MyViewHolder> {

    private List<SongModalClass> songModalClassList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_song_name, txt_artist_name, txt_movie_name,txt_time;
        public ImageView img;


        public MyViewHolder(View view) {
            super(view);

            txt_song_name = (TextView) view.findViewById(R.id.txt_song_name);
            img = (ImageView) view.findViewById(R.id.img);
            txt_artist_name = (TextView) view.findViewById(R.id.txt_artist_name);
            txt_movie_name = (TextView) view.findViewById(R.id.txt_movie_name);
            txt_time = (TextView) view.findViewById(R.id.txt_time);

        }
    }

    public SongAdapter(List<SongModalClass> songModalClassList) {
        this.songModalClassList = songModalClassList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_songs, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        SongModalClass modalClass = songModalClassList.get(position);
        holder.txt_song_name.setText(modalClass.getSongName());
        holder.img.setImageResource(modalClass.getImg());
        holder.txt_artist_name.setText(modalClass.getArtistName());
        holder.txt_movie_name.setText(modalClass.getMovieName());
        holder.txt_time.setText(modalClass.getTime());
    }

    @Override
    public int getItemCount() {
        return songModalClassList.size();
    }


}
