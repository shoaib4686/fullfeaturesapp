package simplycodinghub.learnandroidapp.MusicApp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import simplycodinghub.learnandroidapp.MusicApp.Adapter.ArtistAdapter;
import simplycodinghub.learnandroidapp.MusicApp.ModalClass.ArtistModalClass;
import simplycodinghub.learnandroidapp.R;

/**
 * Created by Remmss on 28-08-2017.
 */

public class ArtistFragment extends Fragment implements View.OnClickListener{


    private Context mContext;
    GridView artist_gridview;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artist, container, false);


        artist_gridview = (GridView)view.findViewById(R.id.artist_gridview);

        List<ArtistModalClass> allItems = getAllItemObject();
        ArtistAdapter artistAdapter = new ArtistAdapter(getActivity(), allItems);
        artist_gridview.setAdapter(artistAdapter);

        artist_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "Position: " + position, Toast.LENGTH_SHORT).show();
            }
        });

    return view;
    }

    private List<ArtistModalClass> getAllItemObject(){
        List<ArtistModalClass> items = new ArrayList<>();
        items.add(new ArtistModalClass(R.drawable.ic_gray,"ATIF ASLAM", "23 Songs"));
        items.add(new ArtistModalClass(R.drawable.ic_gray,"ARIJIT SINGH", "56 Songs"));
        items.add(new ArtistModalClass(R.drawable.ic_gray,"SHREYA GOSHAL", "122 Songs"));
        items.add(new ArtistModalClass(R.drawable.ic_gray,"ARMAN MALIK", "44 Songs"));
        items.add(new ArtistModalClass(R.drawable.ic_gray,"AR REHMAN", "188 Songs"));
        items.add(new ArtistModalClass(R.drawable.ic_gray,"HONEY SINGH", "77 Songs"));
        items.add(new ArtistModalClass(R.drawable.ic_gray,"MIKKA SINGH", "120 Songs"));
        items.add(new ArtistModalClass(R.drawable.ic_gray,"HIMESH RESHMIYA", "90 Songs"));
        items.add(new ArtistModalClass(R.drawable.ic_gray,"SINGER", "67 Songs"));


        return items;
    }

    @Override
    public void onClick(View view) {

    }
}
