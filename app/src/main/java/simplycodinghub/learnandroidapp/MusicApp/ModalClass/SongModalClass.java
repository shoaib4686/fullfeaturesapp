package simplycodinghub.learnandroidapp.MusicApp.ModalClass;

/**
 * Created by Remmss on 29-08-2017.
 */

public class SongModalClass {

    int img;
    String SongName;
    String ArtistName;
    String MovieName;
    String time;


    public SongModalClass(int img, String songName, String artistName, String movieName, String time) {
        this.img = img;
        SongName = songName;
        ArtistName = artistName;
        MovieName = movieName;
        this.time = time;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getSongName() {
        return SongName;
    }

    public void setSongName(String songName) {
        SongName = songName;
    }

    public String getArtistName() {
        return ArtistName;
    }

    public void setArtistName(String artistName) {
        ArtistName = artistName;
    }

    public String getMovieName() {
        return MovieName;
    }

    public void setMovieName(String movieName) {
        MovieName = movieName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
