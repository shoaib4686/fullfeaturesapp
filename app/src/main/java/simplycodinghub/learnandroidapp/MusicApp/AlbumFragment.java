package simplycodinghub.learnandroidapp.MusicApp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import simplycodinghub.learnandroidapp.MusicApp.Adapter.CustomAdapter;
import simplycodinghub.learnandroidapp.MusicApp.ModalClass.MusicModalClass;
import simplycodinghub.learnandroidapp.R;

/**
 * Created by Remmss on 28-08-2017.
 */

public class AlbumFragment extends Fragment implements View.OnClickListener{


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_album, container, false);


        GridView gridview = (GridView)view.findViewById(R.id.gridview);
        SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekbar);




        List<MusicModalClass> allItems = getAllItemObject();
        CustomAdapter customAdapter = new CustomAdapter(getActivity(), allItems);
        gridview.setAdapter(customAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "Position: " + position, Toast.LENGTH_SHORT).show();
            }
        });





// get min and max text view
        final TextView tvMin = (TextView) view.findViewById(R.id.tvmin);
        final TextView tvMax = (TextView) view.findViewById(R.id.tvmax);


    return view;
    }

    private List<MusicModalClass> getAllItemObject(){
        List<MusicModalClass> items = new ArrayList<>();
        items.add(new MusicModalClass(R.drawable.sultan,"SULTAN", "2016"));
        items.add(new MusicModalClass(R.drawable.airlift,"AIRLIFT", "2016"));
        items.add(new MusicModalClass(R.drawable.rockyhandsome,"ROCKY HANDSOME", "2016"));
        items.add(new MusicModalClass(R.drawable.udatapunjab,"UDTA PUNJAB", "2016"));
        items.add(new MusicModalClass(R.drawable.mohenjodaro,"MOHENJO DARO", "2016"));
        items.add(new MusicModalClass(R.drawable.ramanraghav,"RAMAN RAGHAV", "2016"));
        items.add(new MusicModalClass(R.drawable.sultan,"SULTAN", "2016"));
        items.add(new MusicModalClass(R.drawable.airlift,"AIRLIFT", "2016"));
        items.add(new MusicModalClass(R.drawable.rockyhandsome,"ROCKY HANDSOME", "2016"));
        items.add(new MusicModalClass(R.drawable.udatapunjab,"UDTA PUNJAB", "2016"));
        items.add(new MusicModalClass(R.drawable.ic_gray,"MOHENJO DARO", "2016"));
        items.add(new MusicModalClass(R.drawable.ic_gray,"RAMAN RAGHAV", "2016"));


        return items;
    }

    @Override
    public void onClick(View v) {

    }
}
